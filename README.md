# Scarlet Scripts

Scarlet provides users with a computer in cloud. This repository contains scripts that help users host Scarlet on a server and access it for their daily needs.

These scripts are feature rich and take care of basic functionalities.

### Features

#### Client

- [ ] Install needed packages
- [ ] Mouse Support
- [ ] WiFi selection
  - [ ] Show top 10 available networks
  - [ ] Input password for selected network
  - [ ] Connect with the selected network
  - [ ] Notify if the internet connection breaks
  - [ ] Show available network screen if the internet connection breaks
- [ ] User Authentication
  - [x] Input username and password
  - [ ] Send API request to Scarlet Junction
  - [ ] Fetch IP of the Scarlet instance
- [ ] Scarlet
  - [ ] Copy Scarlet client binaries
  - [ ] Start after fetching IP
  - [ ] Show loading screen while establishing connection
  - [ ] Show interactive window to reconnect or shut down if the connection breaks
  - [ ] Confirm Scarlet instance shutdown using API request to Scarlet Junction
  - [ ] Show loading screen while powering off the computer
  - [ ] Poweroff computer if the Scarlet instance is turned off
- [ ] Setup reverse SSH for remote update of computer
- [ ] Add Rebhu Computing logo where possible

##### Junction
- [ ] Setup SSH to accept connections from remote computer

##### Server

- [ ] Install required dependencies
- [ ] Copy Scarlet server binaries
- [ ] Enable hibernation
  - [ ] Check system for hibernation
  - [ ] Hibernate if user is inactive for some time
- [ ] Start Scarlet on boot
- [ ] Enable remote hibernation
- [ ] Enable remote shutdown

### TODO

##### Client

- [ ] Migrate SSH based update to RPM repository
- [ ] Select Bluetooth devices like speakers
- [ ] Enable using local machine for basic tasks in case of poor connection
  - [ ] Copy files from local system to Scarlet instance
  - [ ] Copy local system's running processes and start them on Scarlet instance
- [ ] Enable local webcam for access by Scarlet instance
- [ ] Enable local printer to accept remote prining tasks by Scarlet instance

##### Server

- [ ]