#!/bin/bash
aws configure<<EOF
${aws_access_key_id}
${aws_secret_access_key}
${region}
${output}
EOF

uvicorn app.main:app --host 0.0.0.0 --port 5000