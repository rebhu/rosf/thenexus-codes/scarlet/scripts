```
├── app                  # "app" is a Python package  
│   ├── __init__.py      # this file makes "app" a "Python package"  
│   ├── main.py          # "main" module, e.g. import app.main  
│   ├── config.py        # "configurations for environment variables"  
│   └── models           # "models" is a "Python subpackage"  
│   │   ├── __init__.py  # makes "routers" a "Python subpackage"  
│   │   └── db_model.py  # "db_model" submodule  
│   └── routers          # "routers" is a "Python subpackage"  
│   │   ├── __init__.py  # makes "routers" a "Python subpackage"  
│   │   ├── auto.py      # "auto" submodule  
│   └── utils            # "utils" is a "Python subpackage"  
│   │   ├── __init__.py  # makes "routers" a "Python subpackage"  
│   │   ├── support.py   # "support" submodule  
```

## How to run form CLI

1. Create Python3.9 virtual environment
```
python3.9 -m venv venv
```

2. Git clone repo
```
git clone https://gitlab.com/rebhu/rosf/thenexus-codes/scarlet/scripts
```

3. Create .env file in `app` directory
```
AWS_REGION=
HOSTED_ZONE_ID=
API_KEY=
```

4. Run the app
```
source venv/bin/activate
cd scripts/automation

uvicorn app.main:app --reload --port 5000
```

## Run in a container
1. There is a container file present

2. Create .env_container and .env_app filesin `automation` directory  
.env_container
```
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
REGION=
OUTPUT=
```
.env_app
```
AWS_REGION=
HOSTED_ZONE_ID=
API_KEY=
```

3. Build the container
```
podman build -t auto-api -f Containerfile
```

4. Run the container
```
podman run -d --env-file .env_container --env-file .env_app -p 5001:5000 auto-api
```