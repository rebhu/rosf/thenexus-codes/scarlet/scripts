######################################################3F######################################
# READ THIS: push this code to https://gitlab.com/rebhu/rosf/thenexus-codes/scarlet/junction #
# OPENED THIS REPO IN THE BROWSER AS WELL -- greenz1                                         #
######################################################3#######################################

import string
import random
import boto3
from fastapi import HTTPException

from ..config import settings

# from ..models.db_model import accounts, ports, database

async def fetch_instances():
    
    # get EC2 region
    AWS_REGION = settings.aws_region

    # select resource
    EC2_RESOURCE = boto3.resource('ec2', region_name=AWS_REGION)

    # get all instances
    instances = EC2_RESOURCE.instances.all()

    instance_list = []
    # loop in instances to get the tags and user
    for instance in instances:

        tags = instance.tags       
        for tag in tags:
            tag_key = tag["Key"]
            if tag_key == "user":
                user = tag["Value"]
            if tag_key == "Name":
                name = tag["Value"]
        
        instance_data = {
            "instance_id": instance.id,
            "user": user,
            "state": instance.state["Name"],
            "name": name,
            "public_ip": instance.public_ip_address,
        }
    
        instance_list.append(instance_data)
    
    return instance_list


# INSTANCE STATUS #########################################
async def get_status(instance_id):
    instance = instance_details(instance_id)

    # get instance status
    status = instance.state["Name"]

    tags = instance.tags
    for tag in tags:
        if tag["Key"] == "user":
            user = tag['Value']

    return {"instance_id": instance_id, "user": user, "status": status}


# START INSTANCE #########################################
async def start_instance(instance_id, record=True):
    instance = instance_details(instance_id)

    # get instance status
    status = instance.state["Name"]

    if status == "pending":
        return {"status": status, "message": "Starting instance"}

    if status == "running":
        return {"status": status, "message": "Already running"}

    # if instance is not running then start 
    instance.start()
    instance.wait_until_running()

    # Add Route-53
    if record:
        for tag in instance.tags:
            if tag["Key"] == "Name":
                name = tag["Value"]
        
        record_action(instance.public_ip_address, name, 'CREATE')
    
    return {"status": "running", "message": "Instance started", "public_ip": instance.public_ip_address}


# STOP INSTANCE #########################################
async def stop_instance(instance_id):
    instance = instance_details(instance_id)

    # get instance status
    status = instance.state["Name"]

    if status == "stopping":
        return {"status": status, "message": "Stopping instance"}

    if status == "stopped":
        return {"status": status, "message": "Already stopped"}

    # Delete Route-53
    for tag in instance.tags:
        if tag["Key"] == "Name":
            name = tag["Value"]
    
    record_action(instance.public_ip_address, name, 'DELETE')

    # if instance is not running then start 
    instance.stop()
    instance.wait_until_stopped()

    
    return {"status": "stopped", "message": "Instance stopped"}


# START INSTANCE BY AN-CODE #########################################
async def start_instance_by_an_code(ancode):
    
    # fetch all instances
    instances=await fetch_instances()
    instance_id = ''
    for instance in instances:
        if instance['name'] == ancode:
            instance_id = instance['instance_id']
            state = instance['state']
            public_ip = instance['public_ip']

    if not instance_id:
        raise HTTPException(
            status_code=403, detail="Could not validate AN-ID"
        )
    
    # check state
    if not state == 'stopped':
        return {"status": state, "public_ip": public_ip, "flag": "1"}
    
    # start instance
    instance_details = await start_instance(instance_id=instance_id, record=False)

    return instance_details

# ----------------------------------------------------------------------------------------
# ADD ROUTE #########################################
def record_action(public_ip, name, action):

    client = boto3.client('route53')
    client.change_resource_record_sets(
    ChangeBatch={
        'Changes': [
            {
                'Action': action,
                'ResourceRecordSet': {
                    'Name': '{}.rebhu.net'.format(name),
                    'ResourceRecords': [
                        {
                            'Value': public_ip,
                        },
                    ],
                    'TTL': 300,
                    'Type': 'A',
                },
            },
        ],
        'Comment': 'Customer',
    },
    HostedZoneId=settings.hosted_zone_id,
    )
    resp = {
        "public_ip": public_ip,
        "name": name
    }
    return resp


# SUPPORT FUNCTIONS
def instance_details(instance_id):
    # get EC2 region
    AWS_REGION = settings.aws_region

    # select resource
    EC2_RESOURCE = boto3.resource('ec2', region_name=AWS_REGION)

    # get instance
    instance = EC2_RESOURCE.Instance(instance_id)

    return instance