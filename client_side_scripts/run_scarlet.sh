#!/bin/bash

i=0
# check network connectivity
while true
do
http_code=`curl -Is www.google.com | head -1 | awk '{print $2}'`

if [[ "$http_code" != "200" ]]; then
echo "Waiting for network..."
sleep 2
i=`echo $i | awk '{print $1+1}'`
else
echo "**** Connected ****"
break
fi
done

# ------------------------- START THE SERVER -------------------------
pass="ss13ms110"
anid="2xl-c4i"
http_resp=$(curl --progress-bar -X 'POST' \
	    'http://rebhu.com:5000/api/automation/code/instances/an-code' \
	        -H 'accept: application/json' \
		    -H "access_token: ${pass}" \
		        -H 'Content-Type: application/json' \
			    -d '{"an_code": "'"${anid}"'"}')

status=`echo $http_resp | grep -o '"status":"[^"]*' | grep -o '[^"]*$'`
public_ip=`echo $http_resp | grep -o '"public_ip":"[^"]*' | grep -o '[^"]*$'`
flag=`echo $http_resp | grep -o '"flag":"[^"]*' | grep -o '[^"]*$'`

if [ ! ${status} == 'running' ]
then
	exit
fi
# _-------------------------------------------------------------------

if [[ -z $flag ]]; then
	sleep 7
fi

cd /opt/scarlet
exec ./ga-client config/client.abs.conf rtsp://${public_ip}:8554/desktop
