#!/bin/bash

# wait for netrwork to connect
sleep 5
i=0
# check network connectivity
while true
do
http_code=`curl -Is www.google.com | head -1 | awk '{print $2}'`

if [[ "$http_code" != "200" ]]; then
echo "Waiting for network..."
sleep 2
i=`echo $i | awk '{print $1+1}'`
else
echo "**** Connected ****"
break
fi
done

# run GA command
cd ~/scarlet/base/bin
./ga-client config/client.abs.conf rtsp://net.ss13ms110.rebhu.com:8554/desktop
