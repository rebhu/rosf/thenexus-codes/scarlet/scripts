# server-side setup

1. Install gnome-shell plugin argos and enable in gnome-tweaks<br />
    `path`: https://github.com/mwilck/argos<br />
    `do`: Edit the file "button.js" (located in ~/.local/share/gnome-shell/extensions/argos@pew.worldwidemann.com)<br />
 ```   	/*
 	*     if (dropdownLines.length > 0)
 	*       this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
 	* 
 	*     let menuItem = new PopupMenu.PopupMenuItem(this._file.get_basename(), {
 	*       style_class: "argos-menu-item-edit"
 	*     });
 	*     menuItem.connect("activate", Lang.bind(this, function() {
 	*       Gio.AppInfo.launch_default_for_uri("file://" + this._file.get_path(), null);
 	*     }));
 	*     this.menu.addMenuItem(menuItem);
 	*/
 ```
2. Create power-off extension<br />
     `do`: copy 'power_ext.sh' to ~/.config/argos/<br />
	
     2.1: `do`: restart gnome shell
     	  `run`: alt+f2 enter r and press enter
     	  
3. Install mosquitto
    `run`: sudo apt-get install mosquitto-clients
    
4. Remove user control buttons<br />
    4.1. Move gnome bar at the bottom<br />
        `do`: Install gnome extension 'dash-to-panel@jderose9.github.com'
    
    4.2. Expand sub-menu<br />
    	`do`: Install gnome extension 'BringOutSubmenuOfPowerOffLogoutButton@pratap.fastmail.fm'
    
    4.3. Copy and compile schemas [goto .../BringOutSubmenuOfPowerOffLogoutButton.../schemas]
    	`do`: sudo cp org.gnome.shell.extensions.brngout.gschema.xml /usr/share/glib-2.0/schemas
    	`do`: cd /usr/share/glib-2.0/schemas
    	`run`: glib-compile-schemas .
    
    4.4. Disable poweroff/restart/suspend buttons<br />
    	`run`: gsettings set org.gnome.shell.extensions.brngout remove-logout-button true<br />
    	`run`: gsettings set org.gnome.shell.extensions.brngout remove-power-button true<br />
    	`run`: gsettings set org.gnome.shell.extensions.brngout remove-suspend-button true<br />
    	`run`: gsettings set org.gnome.shell.extensions.brngout remove-restart-button true<br />
    	
    4.5. Disable Lock button<br />
    	`run`: gsettings set org.gnome.desktop.lockdown disable-lock-screen true
    
    
    

