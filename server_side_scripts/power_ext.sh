#!/usr/bin/env bash

echo "Power Off | iconName=system-reboot"
echo "---"
echo "Adios! (<span color='yellow'>Power Off</span>) | bash='mosquitto_pub -h net.dev.rebhu.com -p 8555 -m "0" -t client_ss13ms110 -q 2' terminal=false"
echo "Revive! (<span color='yellow'>Reboot</span>) | bash='mosquitto_pub -h net.dev.rebhu.com -p 8555 -m "1" -t client_ss13ms110 -q 2' terminal=false"
