#!/bin/bash

# set parameters
kill_time=1800		# in seconds

idle_time=$(xprintidle)
kill_time_in_ms=$((kill_time*1000))

# check if idle time is more than threshold
if [[ $idle_time -gt $kill_time_in_ms ]]; then
	sudo poweroff
fi

