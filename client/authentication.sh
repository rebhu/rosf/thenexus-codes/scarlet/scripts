#!/bin/bash

# example script to receive username and password 
# as user inputs in a shell dialogbox

# prerequisite: sudo apt-get install dialog/sudo dnf install dialog

OUTPUTtmp="/tmp/.userinp"

# show a username dialog box
dialog --title "Please enter your Rebhu Computing username" \
--backtitle "Rebhu Computing" \
--inputbox "Username" 8 70 2>$OUTPUTtmp

# get the response
resp=$?

# make a decision
case $resp in
0)
# show a username dialog box
	echo >>$OUTPUTtmp
	dialog --title "Please enter your Rebhu Computing password" \
	--backtitle "Rebhu Computing" \
	--passwordbox "Password (password is invisible while typing)" 8 70 2>>$OUTPUTtmp

# get the response
	resp2=$?

# get the stored data
	username=`awk "NR==1" $OUTPUTtmp`
	password=`awk "NR==2" $OUTPUTtmp`
	echo

# make a decision
	case $resp2 in
	0)
		clear
		echo "Username and password | $username and $password"
		;;
	1)
		clear
		echo "canceled..."
		;;
	255)
		clear
		echo "Escaped..."
	esac
	;;
	
1)
	clear
	echo "canceled..."
	;;
255)
	clear
	echo "Escaped..."
esac



# remove the output file
rm $OUTPUTtmp
	
